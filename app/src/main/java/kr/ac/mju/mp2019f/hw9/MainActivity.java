package kr.ac.mju.mp2019f.hw9;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private CustomArrayAdapter adapter;

    private ArrayList<Todo> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.listView = findViewById(R.id.listView);

        findViewById(R.id.fabAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // 추가폼 호출
                showAddDialog();
            }
        });

        // 리스트 구성
        listData();
    }

    /* 추가폼 호출 */
    private void showAddDialog() {
        // AlertDialog View layout
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.layout_todo_add, null);

        new AlertDialog.Builder(this)
                .setPositiveButton("ADD", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(@NonNull DialogInterface dialog, int which) {
                        // 추가
                        String name = ((EditText) layout.findViewById(R.id.editName)).getText().toString();
                        if (TextUtils.isEmpty(name)) {
                            Toast.makeText(MainActivity.this, "TODO Name empty", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        String priority = ((EditText) layout.findViewById(R.id.editPriority)).getText().toString();
                        if (TextUtils.isEmpty(priority)) {
                            Toast.makeText(MainActivity.this, "Priority empty", Toast.LENGTH_SHORT).show();
                            return;
                        }

                        // 데이터 추가
                        addData(name, priority);
                    }
                })
                //.setNegativeButton("CANCEL", null)
                //.setCancelable(false)
                .setCancelable(true)
                .setTitle("Add new TODO")
                .setView(layout)
                .show();
    }

    /* 리스트 구성 */
    private void listData() {
        this.items = new ArrayList<>();

        // SQLite 사용
        DBHelper dbHelper = DBHelper.getInstance(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        try {
            // 쿼리문
            String sql = "SELECT tID, name, priority FROM todos";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                // 데이터
                Todo todo = new Todo(cursor.getInt(cursor.getColumnIndex("tID")),
                        cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("priority")));

                this.items.add(todo);
            }

            cursor.close();
        } catch (SQLException e) {}

        db.close();

        // 리스트 구성
        this.adapter = new CustomArrayAdapter(this, this.items);
        this.listView.setAdapter(this.adapter);
    }

    /* 추가 */
    private void addData(String name, String priority) {
        // SQLite 사용
        DBHelper dbHelper = DBHelper.getInstance(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        try {
            // 등록
            Object[] args = { name, priority };
            String sql = "INSERT INTO todos(name, priority) VALUES(?,?)";

            db.execSQL(sql, args);

            // 리스트 새로고침
            listData();

        } catch (SQLException e) {}

        db.close();
    }
}
