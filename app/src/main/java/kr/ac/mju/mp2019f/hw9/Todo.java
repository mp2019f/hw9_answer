package kr.ac.mju.mp2019f.hw9;

public class Todo {

    // id
    private int id;

    private String name;
    private String priority;

    public Todo(int id, String name, String priority) {
        this.id = id;
        this.name = name;
        this.priority = priority;
    }

    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPriority() {
        return this.priority;
    }
}
